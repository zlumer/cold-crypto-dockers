# Docker files for Cold Crypto fast initialization

## Installation
```
git clone git@gitlab.com:zlumer/cold-crypto-dockers.git
git submodule update --init --recursive
```

To update submodules, use:
`git submodule foreach --recursive git pull origin master`

## Running:
```
WEBRTC_ADDR=<your local ip> docker-compose up
```

## Geofenced beacon payments
You can easily pay at any supported café or restaurant. For demo purposes we will open café website directly. In real world there will be a QR code printed near your table (or NFC/Bluetooth device) that will redirect you to café website.
1. Install Cold Crypto Mobile on your iOS device.
2. Open café website example (`http://<your local ip address>:8080`) on your mobile.
3. Pressing `Pay now` button on café website will redirect you to Cold Crypto Web Wallet (`http://<your ip>:4444`).
4. Pressing `Sign` button on Cold Crypto Web Wallet will open Cold Crypto Mobile Signer App via native App Link.
5. Pressing `Sign` button in mobile signer app will redirect you back to café website (`http://<your ip>:8080`).

## Manual payments
If you wish to send a transaction manually, you can use Cold Crypto Web Wallet.
1. Open Cold Crypto Web Wallet (`http://<your ip>:4444`).
2. Press `Login via QR Code` if you want to login from an air-gapped device or if you are using React Native app [prototype](https://github.com/DucaturFw/cold-crypto-mobile-prototype).
3. Press `WebRTC login` to login with an online iOS device running Cold Crypto Mobile App.
4. Scan QR code with Cold Crypto Mobile App (iOS only) or React Native signer prototype app (iOS and Android, but no WebRTC support and hard-coded wallets).
5. Follow on-screen instructions: create transactions, sign them and push into blockchains.