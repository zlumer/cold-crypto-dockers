#!/bin/bash
export WEBRTC_ADDR=$({ ifconfig en0 2>/dev/null || ifconfig wifi0 2>/dev/null; } | grep inet | grep -v inet6 | awk '{print $2}')
# echo $WEBRTC_ADDR
docker-compose up --build
# docker-compose config
